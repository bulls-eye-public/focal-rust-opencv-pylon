FROM ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive


ARG PYLON_VERSION=7_4_0_14900

# Use . on one of the separators for the deb file instead of _ as in the gz file
ARG DEB_PYLON_VERSION=7.4.0.14900

ARG PYLON_URL=https://www2.baslerweb.com/media/downloads/software/pylon_software/pylon_${PYLON_VERSION}_linux_x86_64_debs.tar.gz


RUN apt-get update && \
    apt-get install -y curl wget build-essential libclang-dev clang libopencv-dev && \
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y && \
    . $HOME/.cargo/env && \
    cargo install cargo-deb


ENV PATH="/root/.cargo/bin:${PATH}"
WORKDIR /workspace



RUN curl -o pylon_${PYLON_VERSION}_linux_x86_64_debs.tar.gz ${PYLON_URL} && \
    tar -xzf pylon_${PYLON_VERSION}_linux_x86_64_debs.tar.gz && \
    dpkg -i pylon_${DEB_PYLON_VERSION}-deb0_amd64.deb && \
    rm -f pylon_${PYLON_VERSION}_linux_x86_64_debs.tar.gz pylon_${DEB_PYLON_VERSION}-deb0_amd64.deb codemeter*.deb INSTALL
